/*
  © Copywrite Jason Butler 2017 GPL
*/

module fulladder
(
  input x,
  input y,
  input cin,

  output reg A,
  output reg cout
);

reg p,q;
always @(x,y,cin)
begin
assign {cout,A} = cin + y + x;
end
endmodule
