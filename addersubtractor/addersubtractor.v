/*
  © Copywrite Jason Butler 2017 GPL
*/

module fulladdersub32
(
  input [31:0] in1,
  input [31:0] in2,
  input select,
  output wire [31:0] out1,
  output wire cout
);

wire [32:0] carries;
wire [32:0] input_temp;
assign carries[0] = select;

genvar i;
generate 
  for (i=0; i<32; i =i+1) begin:fulladders
    xor U1(input_temp[i], in1[i], select);
    fulladder s(input_temp[i], in2[i], carries[i], out1[i], carries[i+1]);
end endgenerate
assign carryout = carries[32];
endmodule
