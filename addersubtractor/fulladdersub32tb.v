/*
  © Copywrite Jason Butler 2017 GPL
*/

`timescale 1ns / 100ps

module fulladdertb;

reg [31:0] input1;
reg [31:0] input2;
reg carryin;

wire [31:0] out;
wire carryout;
integer i;
integer j;
reg select;

fulladdersub32 uut (
.in1(input1),
.in2(input2),
.select(select),
.out1(out),
.cout(carryout)
);

initial
begin
  $dumpfile("fulladdertb.vcd");
  $dumpvars(0,fulladdertb);
select = 0;
input1 = 0;
input2 = 0;
carryin = 0;
#20; input1=1;
//generate
for(i=0; i<=31; i=i+1) begin
  #20; input1=i;
  for(j=0; j<=31; j=j+1) begin
  #20; input2=j;
  end
end
#20; input1=0;
#20; input2=0;
#20; select=1;
for(i=0; i<=31; i=i+1) begin
  #20; input1=i;
  for(j=0; j<=31; j=j+1) begin
  #20; input2=j;
  end
end
#40;
/*
#20; input1 =1;
#20; input2 =1;
#20; input1 =0;
#20; carryin =1;
#20; carryin =0;
#20; input2=0;
#20; input1=1;
#20; input2=1;
#20; input1=2;
#20; input2=0;
#20; input2=1;
#20; input2=2;
#40;
*/
end

initial
begin
$monitor("time = %2d, CIN=%1b, IN1=%32b, IN2=%32b, COUT=%1b,OUT=%32b",
$time,carryin,input2,input1,carryout,out);
end

endmodule
